# Week 3 Mini Project

## Requirements
Create S3 bucket using AWS CDK

Use CodeWhisperer to generate CDK code

Add bucket properties like versioning and encryption

## Steps
1. In CodeCatalyst, create a new project.
2. Set up a new empty development environment in Cloud9.
3. In the AWS Management Console, add a new IAM user with permissions attched with policies 
    * `AmazonS3FullAccess`
    * `AWSCloudFormationFullAccess`
    * `IAMFullAccess`
    * `AWSCloud9User`
    * `AWSCodeBuildDeveloperAccess`
    * `AWSLambda_FullAccess`
4. Complete the IAM user setup and go to the "Security Credentials" section to generate an access key.
5. Store the generated AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.
6. Return to the Cloud9 environment and input `aws configure` in the terminal.
7. When prompted, enter the previously stored AWS credentials and retain the default settings for the region and output format.
8. In the terminal, create a new directory for your project and navigate into it.
9. Initialize a new CDK project in TypeScript using `cdk init app --language typescript`.
10. Ultilize CodeWhisperer to generate code for a S3 Bucket in the `lib` and `bin` directories.
11. Compile the project with `npm run build`.
12. Produce a CloudFormation template using `cdk synth`.
13. Run `cdk bootstrap aws://ACCOUNT-NUMBER/REGION` to set up bootstrap if you are using CDK for the first time.
13. Deploy the generated CloudFormation template with `cdk deploy`.
14. Navigate to the AWS Portal and check if the S3 bucket has been created.

## S3 Bucket Screenshot
![s3](Created_S3_Bucket.png)
## Versioning enabled
![version](versioning.png)
## Encryption enabled
![Encryption](encryption.png)
